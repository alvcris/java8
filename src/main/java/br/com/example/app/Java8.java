package br.com.example.app;


import br.com.example.features.*;
import br.com.example.features.Interfaces.FunctionalInterfaceExample;
import br.com.example.features.Interfaces.MyString;

/**
 * https://www.journaldev.com/2389/java-8-features-with-examples
 * https://medium.freecodecamp.org/learn-these-4-things-and-working-with-lambda-expressions-b0ab36e0fffc
 *
 *  forEach() method in Iterable interface
 *  default and static methods in Interfaces
 *  Functional Interfaces and Lambda Expressions
 *  Java Stream API for Bulk Data Operations on Collections
 *  Java Time API
 *  Collection API improvements
 *  Concurrency API improvements
 *  Java IO improvements
 *  Miscellaneous Core API improvements
 *
 */
public class Java8
{
    public static void main(final String[] args)
    {

        //ForEach example
        ForEach.forEachAndLists();
        ForEach.forEachAndMap();

        //Default And Static Methods
        DefaultAndStaticMethodsImpl a = new DefaultAndStaticMethodsImpl();
        a.defaultMethod();
        a.defaultMethod2();


        //FunctionalInterfacesImpl
        FunctionalInterfaceExample fi = () -> {
            System.out.println("FunctionalInterfaceExample");
        };
        fi.print();

        //Lambda Expressions
        LambdaExpressions le = new LambdaExpressions();
        System.out.println(le.lambdaExpressionIsEven(10));
        System.out.println(le.lambdaExpressionIsNegative(5));
        System.out.println(le.lambdaExpressionEveningGreeting("test"));
        System.out.println(le.lambdaExpressionMorningGreeting("test"));

        //Block Lambda Expressions
        System.out.println(le.lambdaExpressionReverseStr("etset"));

        //Generic Functional Interfaces
        System.out.println(le.lambadaExpressionFactorial(5));

        //Lambda Expressions as arguments
        //Generic Functional Interfaces
        System.out.println(reverseStr(le.revert(), "gnirts"));

        //Stream
        StreamAPI.generalExample();
        StreamAPI.intStreamAsLoop();



    }

    public static String reverseStr(MyString reverse, String str){
        return reverse.myStringFunction(str);
    }
}
