package br.com.example.app;

import java.util.*;

public class Teste {
    public static void main(String[] args) {
        int [] array = {1,2,3,4,5,6};
        int numberOfRotations = 2;
        int [] arrayRotated = array;


        for (int i = 0; i < numberOfRotations;i++){
            arrayRotated = rotate(arrayRotated);
        }
        System.out.println(arrayRotated);

        // S T R I N G S
        // S G N I R T S

        // T H I S I S A S T R I N G
        // S N I R T S A S I S I H T

        // C A S A D O C A R A L H O
        // O H L A R A C O D A S A C

        rotateString("C A S A D O C A R A L H O");

    }

    private static void rotateString(String string){
        System.out.println(string);

        String reverse= "";

        for(int i = string.length()-1; i>=0;i--){
            reverse = reverse + string.charAt(i);
        }
        System.out.println(reverse);
    }

    private static int[] rotate(int[] array) {
        int [] rotated = new int[array.length];
        int firstElement = array[0];

        for (int i=0; i< array.length-1; i++){
            rotated[i] = array[i+1];
        }
        rotated[array.length-1] = firstElement;
        return rotated;
    }
}
