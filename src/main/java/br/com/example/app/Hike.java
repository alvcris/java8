package br.com.example.app;

public class Hike {

    public static void main(String[] args) {
        System.out.println(countingValleys(8, "UDDDUDUU"));
    }

    static int countingValleys(int n, String s) {
        int level = 0;
        int valley = 0;

        char[] chars = s.toCharArray();

        for(char c : chars){
            if(c == 'U'){
                level++;
            } else if(c == 'D'){
                if (level == 0) {
                    valley++;
                }
                level--;
            }

        }

        return valley;
    }

}
