package br.com.example.features;

import br.com.example.features.Interfaces.FunctionalInterfaceExample;

/**
 * https://www.journaldev.com/2389/java-8-features-with-examples#functional-interface-lambdas
 *
 * An interface with exactly one abstract method becomes Functional Interface.
 * We don’t need to use @FunctionalInterface annotation to mark an interface as
 * Functional Interface. @FunctionalInterface annotation is a facility to avoid
 * accidental addition of abstract methods in the functional interfaces.
 */
public class FunctionalInterfacesImpl implements FunctionalInterfaceExample {

    @Override
    public void print() {
        System.out.println("Functional Interface");
    }
}