package br.com.example.features;

import br.com.example.Models.Bar;
import br.com.example.Models.Foo;
import br.com.example.Models.Person;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * http://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/
 * A stream represents a sequence of elements and supports different kind of
 * operations to perform computations upon those elements
 */
public class StreamAPI {

    private static List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");

    /**
     * 1 - create a stream (intermediate operation)
     * 2 - filter by only those will start with "c" (intermediate operation)
     * 3 - using map, return a new list with all items as uppercase (intermediate operation)
     * 4 - sort it out (intermediate operation)
     * 5 - print all items (terminal operation)
     */
    public static void generalExample() {
        myList
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);
    }

    //IntStream as loop
    public static void intStreamAsLoop() {
        IntStream.range(1, 5).forEach(p -> System.out.println(p));
        IntStream.range(1, 5).mapToObj(i -> "a" + 1).forEach(System.out::println);
    }

    private static List<Person> persons =
            Arrays.asList(
                    new Person("Max", 18),
                    new Person("Peter", 23),
                    new Person("Pamela", 23),
                    new Person("David", 12));

    //Collect
    //Collect is an extremely useful terminal operation to transform the elements of the stream
    // into a different kind of result, e.g. a List, Set or Map. Collect accepts a Collector which
    // consists of four different operations: a supplier, an accumulator, a combiner and a finisher.
    public static List<Person> collectAsList() {
        List<Person> filtered = persons.stream()
                .filter(p -> p.name.startsWith("P"))
                .collect(Collectors.toList());
        return filtered;
    }

    public static Map<Integer, List<Person>> collectAsMap() {
        Map<Integer, List<Person>> personsByAge = persons
                .stream()
                .collect(Collectors.groupingBy(p -> p.age));

        personsByAge
                .forEach((age, p) -> System.out.format("age %s: %s\n", age, p));

        return personsByAge;
    }

    //In order to transform the stream elements into a map, we have to specify how both the keys and the values should be mapped
    public static Map<Integer, String> collectListToMap() {
        Map<Integer, String> map = persons
                .stream()
                .collect(Collectors.toMap(
                        p -> p.age,
                        p -> p.name));
        return map;
    }

    //let's try to build our own special collector. We want to transform all persons of the stream into
    // a single string consisting of all names in upper letters separated by the | pipe character. In
    // order to achieve this we create a new collector via Collector.of(). We have to pass the four
    // ingredients of a collector: a supplier, an accumulator, a combiner and a finisher.
    public static void customCollector() {
        Collector<Person, StringJoiner, String> personNameCollector =
                Collector.of(
                        () -> new StringJoiner(" | "),   // supplier
                        (j, p) -> j.add(p.name.toUpperCase()),  // accumulator
                        (j1, j2) -> j1.merge(j2),               // combiner
                        StringJoiner::toString);                // finisher

        String names = persons
                .stream()
                .collect(personNameCollector);
        System.out.println(names);  // MAX | PETER | PAMELA | DAVID
    }

    //FlatMap
    //FlatMap transforms each element of the stream into a stream of other objects. So each object
    // will be transformed into zero, one or multiple other objects backed by streams. The contents
    //of those streams will then be placed into the returned stream of the flatMap operation.
    public static void FlatMap() {
        List<Foo> foos = new ArrayList<>();

        // create foos
        IntStream
                .range(1, 4)
                .forEach(i -> foos.add(new Foo("Foo" + i)));

        // create bars
        foos.forEach(f ->
                IntStream
                        .range(1, 4)
                        .forEach(i -> f.bars.add(new Bar("Bar" + i + " <- " + f.name))));

        //FlatMap accepts a function which has to return a stream of objects.
        //So in order to resolve the bar objects of each foo, we just pass the appropriate function:
        foos.stream()
                .flatMap(f -> f.bars.stream())
                .forEach(b -> System.out.println(b.name));
    }

    //Reduce
    //The reduction operation combines all elements of the stream into a single result. Java 8
    //supports three different kind of reduce methods. The first one reduces a stream of elements
    //to exactly one element of the stream. Let's see how we can use this method to determine
    //the oldest person:
    public static void reduce() {
        persons
                .stream()
                //That's actually a BiFunction where both operands share the same type, in that case
                //Person. BiFunctions are like Function but accept two arguments.
                .reduce((p1, p2) -> p1.age > p2.age ? p1 : p2)
                .ifPresent(System.out::println);    // Pamela
    }

    //The second reduce method accepts both an identity value and a BinaryOperator accumulator.
    //Stream.reduce() method has the following signature –
    //T reduce(T identity, BinaryOperator<T> accumulator);
    //Where,
    //      – identity is initial value of type T which will be used as the first value in the reduction operation.
    //      – accumulator is an instance of a BinaryOperator Function(Functional Interface) of type T.
    //
    //Stream.reduce() is a terminal operation.
    public static void reduceIdentity() {
        Person result =
                persons
                        .stream()
                        .reduce(new Person("", 0), (p1, p2) -> {
                            p1.age += p2.age;
                            p1.name += p2.name;
                            return p1;
                        });

        System.out.format("name=%s; age=%s", result.name, result.age);
    }

    //The third reduce method accepts three parameters: an identity value, a BiFunction accumulator
    //and a combiner function of type BinaryOperator. Since the identity values type is not restricted
    //to the Person type, we can utilize this reduction to determine the sum of ages from all persons:
    public static void reduceIdentityAccumulatorCombiner() {
        Integer ageSum = persons
                .stream()
                .reduce(0, (sum, p) -> sum += p.age, (sum1, sum2) -> sum1 + sum2);

        //combiner (sum1, sum2) -> sum1 + sum2) will be called only during a parallel execution
        System.out.println(ageSum);
    }

}

