package br.com.example.features;

/**
 * https://dzone.com/articles/java-8-default-and-static-methods-in-interfaces
 *
 * This feature enables us to add new functionality in the interfaces without
 * breaking the existing contract of the implementing classes
 */
public class DefaultAndStaticMethodsImpl implements DefaultAndStaticMethods{

    @Override
    public void normalMethod() {

    }
}



interface DefaultAndStaticMethods{
    void normalMethod();

    default void defaultMethod() {
        System.out.println("interface default method");
        System.out.println("interface default method calling " + staticMethod());
    }

    default void defaultMethod2() {
        System.out.println("interface default method");
        System.out.println("interface default method calling " + staticMethod2());
    }

    static String staticMethod() {
        return "staticMethod";
    }

    static String staticMethod2() {
        return "staticMethod";
    }

}
