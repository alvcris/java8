package br.com.example.features;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.Set;

/**
 * https://dzone.com/articles/deeper-look-java-8-date-and
 */

public class TimeAPI {
    /*
     * LocalDate - Date and Time Objects
     * the current date
     */
    LocalDate currentDate = LocalDate.now();

    // months values start at 1 (2014-08-01)
    LocalDate tenthFeb2014 = LocalDate.of(2014, Month.FEBRUARY, 10);
    LocalDate firstAug2014 = LocalDate.of(2014, 8, 1);

    // the 65th day of 2010 (2010-03-06)
    LocalDate sixtyFifthDayOf2010 = LocalDate.ofYearDay(2010, 65);

    /////////////////// LocalTime ///////////////////////
    // 12:00
    LocalTime midday = LocalTime.of(12, 0);

    // 13:30:15
    LocalTime afterMidday = LocalTime.of(13, 30, 15);

    // 12345th second of day (03:25:45)
    LocalTime fromSecondsOfDay = LocalTime.ofSecondOfDay(12345);

    //LocalDateTime
    // dates with times, e.g. 2014-02-18 19:08:37.950
    LocalDateTime currentDateTime = LocalDateTime.now();

    // 2014-10-02 12:30
    LocalDateTime secondAug2014 = LocalDateTime.of(2014, 10, 2, 12, 30);

    // 2014-12-24 12:00
    LocalDateTime christmas2014 = LocalDateTime.of(2014, Month.DECEMBER, 24, 12, 0);

    // information about the month
    // 2014-06-15
    LocalDate date2 = LocalDate.of(2014, 2, 15);

    // false
    boolean isBefore = LocalDate.now().isBefore(date2);

    // FEBRUARY
    Month february = date2.getMonth();

    // 2
    int februaryIntValue = february.getValue();

    // 28
    int minLength = february.minLength();

    // 29
    int maxLength = february.maxLength();

    ///////////////// Time Zones ////////////////////////
    ZoneId losAngeles = ZoneId.of("America/Los_Angeles");
    ZoneId berlin = ZoneId.of("Europe/Berlin");

    // 2014-02-20 12:00
    LocalDateTime dateTime2 = LocalDateTime.of(2014, 02, 20, 12, 0);

    // 2014-02-20 12:00, Europe/Berlin (+01:00)
    ZonedDateTime berlinDateTime = ZonedDateTime.of(dateTime2, berlin);

    // 2014-02-20 03:00, America/Los_Angeles (-08:00)
    ZonedDateTime losAngelesDateTime = berlinDateTime.withZoneSameInstant(losAngeles);

    // -28800
    int offsetInSeconds = losAngelesDateTime.getOffset().getTotalSeconds();

    // a collection of all available zones
    Set<String> allZoneIds = ZoneId.getAvailableZoneIds();

    // using offsets
    LocalDateTime date = LocalDateTime.of(2013, Month.JULY, 20, 3, 30);
    ZoneOffset offset = ZoneOffset.of("+05:00");

    // 2013-07-20 03:30 +05:00
    OffsetDateTime plusFive = OffsetDateTime.of(date, offset);

    // 2013-07-19 20:30 -02:00
    OffsetDateTime minusTwo = plusFive.withOffsetSameInstant(ZoneOffset.ofHours(-2));

    /////////////////// Timestamps //////////////////////////////
    // current time
    Instant now = Instant.now();

    // from unix timestamp, 2010-01-01 12:00:00
    Instant fromUnixTimestamp = Instant.ofEpochSecond(1262347200);

    // same time in millis
    Instant fromEpochMilli = Instant.ofEpochMilli(1262347200000l);

    // parsing from ISO 8601
    Instant fromIso8601 = Instant.parse("2010-01-01T12:00:00Z");

    // toString() returns ISO 8601 format, e.g. 2014-02-15T01:02:03Z
    String toIso8601 = now.toString();

    // as unix timestamp
    long toUnixTimestamp = now.getEpochSecond();

    // in millis
    long toEpochMillis = now.toEpochMilli();

    // plus/minus methods are available too
    Instant nowPlusTenSeconds = now.plusSeconds(10);

    ////////////////// Formatting and Parsing /////////////////////
    // 2014-04-01 10:45
    LocalDateTime dateTime = LocalDateTime.of(2014, Month.APRIL, 1, 10, 45);

    // format as basic ISO date format (20140220)
    String asBasicIsoDate = dateTime.format(DateTimeFormatter.BASIC_ISO_DATE);

    // format as ISO week date (2014-W08-4)
    String asIsoWeekDate = dateTime.format(DateTimeFormatter.ISO_WEEK_DATE);

    // format ISO date time (2014-02-20T20:04:05.867)
    String asIsoDateTime = dateTime.format(DateTimeFormatter.ISO_DATE_TIME);

    // using a custom pattern (01/04/2014)
    String asCustomPattern = dateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

    // french date formatting (1. avril 2014)
    String frenchDate = dateTime.format(DateTimeFormatter.ofPattern("d. MMMM yyyy", new Locale("fr")));

    // using short german date/time formatting (01.04.14 10:45)
    DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
           .withLocale(new Locale("de"));

    String germanDateTime = dateTime.format(formatter);

    // parsing date strings
    LocalDate fromIsoDate = LocalDate.parse("2014-01-20");
    LocalDate fromIsoWeekDate = LocalDate.parse("2014-W14-2", DateTimeFormatter.ISO_WEEK_DATE);
    LocalDate fromCustomPattern = LocalDate.parse("20.01.2014", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
}