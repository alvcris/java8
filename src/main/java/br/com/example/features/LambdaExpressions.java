package br.com.example.features;

import br.com.example.features.Interfaces.MyGeneric;
import br.com.example.features.Interfaces.MyGreeting;
import br.com.example.features.Interfaces.MyString;
import br.com.example.features.Interfaces.NumericTest;

import java.util.stream.IntStream;

/**
 * Lambda Expression are the way through which we can visualize functional
 * programming in  the java object oriented world. Objects are the base of
 * java  programming  language and we can never have a function without an
 * Object,  that’s  why  Java  language  provide  support for using lambda
 * expressions only with functional interfaces.
 *
 * Lambda Expressions syntax is (argument) -> (body).
 */
public class LambdaExpressions {
    NumericTest isEven = (n) -> (n % 2) == 0;
    NumericTest isNegative = (n) -> (n < 0);

    MyGreeting morningGreeting = (str) -> "Good Morning " + str + "!";
    MyGreeting eveningGreeting = (str) -> "Good Evening " + str + "!";

    // Block lambda to reverse string
    MyString reverseStr = (str) -> {
        StringBuilder result = new StringBuilder();

        for(int i = str.length()-1; i >= 0; i--)
            result.append(str.charAt(i));

        return result.toString();
    };

    //Generic Functional Interfaces
    // String version of MyGenericInteface
    MyGeneric<String> reverse = (str) -> {
        String result = "";

        for(int i = str.length()-1; i >= 0; i--)
            result += str.charAt(i);

        return result;
    };

    // Integer version of MyGeneric
    MyGeneric<Integer> factorial = (Integer number) -> {
        int result = 1;

        for(int i=1; i <= number; i++) {
            result = result * i;
        }

        /*
        int result = IntStream.rangeClosed(1, number)
                .reduce(1, (a, b) -> a * b);
        */
        return result;
    };


    public boolean lambdaExpressionIsEven(int n){
        return isEven.computeTest(n);
    }

    public boolean lambdaExpressionIsNegative(int n){
        return isNegative.computeTest(n);
    }

    public String lambdaExpressionMorningGreeting(String s){
        return morningGreeting.processName(s);
    }

    public String lambdaExpressionEveningGreeting(String s){
        return eveningGreeting.processName(s);
    }

    public String lambdaExpressionReverseStr(String s){
        return reverseStr.myStringFunction(s);
    }

    public Integer lambadaExpressionFactorial(Integer i){
        return factorial.compute(i);
    }

    public MyString revert(){
        MyString reverseArg = (str) -> {
            String result = "";

            for(int i = str.length()-1; i >= 0; i--)
                result += str.charAt(i);

            return result;
        };

        return reverseArg;
    }
}
