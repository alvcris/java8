package br.com.example.features.Interfaces;

@FunctionalInterface
public interface MyGreeting {
    String processName(String str);
}
