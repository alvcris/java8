package br.com.example.features.Interfaces;

@FunctionalInterface
public interface NumericTest {
    boolean computeTest(int n);
}
