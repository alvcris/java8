package br.com.example.features.Interfaces;

@FunctionalInterface
public interface MyGeneric<T> {
    T compute(T t);
}
