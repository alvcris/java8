package br.com.example.features;

import java.util.Arrays;
import java.util.List;

public class CollectionAPIImprovements {

    //forEachRemaining(Consumer<? super E> action) : this method takes a Consumer wich represents an action
    //that has to be performed for each remaining element, until all elements have been processed or the
    //action throws an exception.
    //A Consumer interface represents an operation that accepts a single input argument and returns no result.
    //Pretty much a lambda
    public static void forEachRemaining(){
        List<String> list = Arrays.asList("this", "is", "a", "lambda", "bitch");
        list.iterator().forEachRemaining(e -> System.out.print(e+" "));
    }

    //It removes all the elements from the List which satisfies the given Predicate.
    public static void removeIf(){
        List<String> list = Arrays.asList("this", "is", "a", "lambda", "bitch");
        list.removeIf(s -> s.equals("is"));
        list.forEach(System.out::println);
    }
}
